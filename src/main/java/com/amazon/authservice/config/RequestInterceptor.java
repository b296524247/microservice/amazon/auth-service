//package com.amazon.authservice.config;
//
//import org.springframework.stereotype.Component;
//import org.springframework.web.reactive.function.server.ServerRequest;
//import org.springframework.web.server.ServerWebExchange;
//import org.springframework.web.server.WebFilter;
//import org.springframework.web.server.WebFilterChain;
//import reactor.core.publisher.Mono;
//
//@Component
//public class RequestInterceptor implements WebFilter {
//
//    @Override
//    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
//        return chain.filter(exchange)
//                .contextWrite(ctx -> ctx.put(ServerRequest.class,
//                        ServerRequest.create(exchange,exchange.)));
//    }
//}